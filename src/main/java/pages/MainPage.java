package pages;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPage {

    private final SelenideElement header;
    private final SelenideElement footer;

    public MainPage() {
        header = $(".js-Header");
        footer = $(".Footer");
    }

    @Step("Open {url}")
    public MainPage openMainPage(String url) {
        open(url);
        return this;
    }

    @Step("Check header for visibility")
    public MainPage checkHeaderVisible() {
        header.shouldBe(Condition.visible);
        return this;
    }

    @Step("Check footer for visibility")
    public MainPage checkFooterVisible() {
        footer.scrollTo().shouldBe(Condition.visible);
        return this;
    }

    @Step("This case will fail")
    public MainPage generateError() {
        $(".errorrr").shouldBe(Condition.exist);
        return this;
    }
}
